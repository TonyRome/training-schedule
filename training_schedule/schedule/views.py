from django.shortcuts import render
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView
from rest_framework.response import Response
from calendar import HTMLCalendar
from django.views.generic import ListView


class Schedule(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'schedule.html'

    @staticmethod
    def get(request):
        return render(request, 'schedule.html')


class CalendarView(ListView):
    template_name = 'calendar.html'
