from django.urls import path
from .views import Schedule

app_name = 'schedule'

urlpatterns = [
    path('home', Schedule.as_view(), name='schedule-home')
]

